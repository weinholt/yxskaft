#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020, 2021 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

(import
  (rnrs)
  (yxskaft simple))

(define width 320)
(define height 240)
(define window (create-simple-window "test-connect.sps" width height))
(define &pixmap (simple-window-ptr window))
(define depth (simple-window-depth window))
(define bytes-per-line (simple-window-bytes-per-line window))

(render-simple-window window)

(let ((e (simple-window-poll-for-event window)))
  (when e
    (display "X event: ")
    (write e)
    (newline)))

(destroy-simple-window window)
