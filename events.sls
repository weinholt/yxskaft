;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2008-2021 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; X events

(library (yxskaft events)
  (export
    event?
    event-synthetic?
    event-code
    event-time

    input-event?
    input-event-detail
    input-event-root
    input-event-event
    input-event-child
    input-event-root-x
    input-event-root-y
    input-event-event-x
    input-event-event-y
    input-event-state
    input-event-same-screen?
    key-press?
    key-release?
    button-press?
    button-release?
    motion-notify?

    enter-notify?
    leave-notify?

    keymap-notify?
    keymap-notify-keys

    expose?
    expose-window
    expose-x
    expose-y
    expose-width
    expose-height
    expose-count

    no-exposure?
    no-exposure-drawable
    no-exposure-minor-opcode
    no-exposure-major-opcode

    mapping-notify?
    mapping-notify-request

    client-message?
    client-message-window
    client-message-type
    client-message-data

    XCB_EVENT_MASK_NO_EVENT
    XCB_EVENT_MASK_KEY_PRESS
    XCB_EVENT_MASK_KEY_RELEASE
    XCB_EVENT_MASK_BUTTON_PRESS
    XCB_EVENT_MASK_BUTTON_RELEASE
    XCB_EVENT_MASK_ENTER_WINDOW
    XCB_EVENT_MASK_LEAVE_WINDOW
    XCB_EVENT_MASK_POINTER_MOTION
    XCB_EVENT_MASK_POINTER_MOTION_HINT
    XCB_EVENT_MASK_BUTTON_1MOTION
    XCB_EVENT_MASK_BUTTON_2MOTION
    XCB_EVENT_MASK_BUTTON_3MOTION
    XCB_EVENT_MASK_BUTTON_4MOTION
    XCB_EVENT_MASK_BUTTON_5MOTION
    XCB_EVENT_MASK_BUTTON_MOTION
    XCB_EVENT_MASK_KEYMAP_STATE
    XCB_EVENT_MASK_EXPOSURE
    XCB_EVENT_MASK_VISIBILITY_CHANGE
    XCB_EVENT_MASK_STRUCTURE_NOTIFY
    XCB_EVENT_MASK_RESIZE_REDIRECT
    XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY
    XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT
    XCB_EVENT_MASK_FOCUS_CHANGE
    XCB_EVENT_MASK_PROPERTY_CHANGE
    XCB_EVENT_MASK_COLOR_MAP_CHANGE
    XCB_EVENT_MASK_OWNER_GRAB_BUTTON

    parse-xevent)
  (import
    (rnrs)
    (struct pack))

(define XCB_KEY_PRESS 2)
(define XCB_KEY_RELEASE 3)
(define XCB_BUTTON_PRESS 4)
(define XCB_BUTTON_RELEASE 5)
(define XCB_MOTION_NOTIFY 6)
(define XCB_ENTER_NOTIFY 7)
(define XCB_LEAVE_NOTIFY 8)
(define XCB_KEYMAP_NOTIFY 11)
(define XCB_EXPOSE 12)
(define XCB_NO_EXPOSURE 14)
(define XCB_CLIENT_MESSAGE 33)
(define XCB_MAPPING_NOTIFY 34)

(define XCB_EVENT_MASK_NO_EVENT 0)
(define XCB_EVENT_MASK_KEY_PRESS 1)
(define XCB_EVENT_MASK_KEY_RELEASE 2)
(define XCB_EVENT_MASK_BUTTON_PRESS 4)
(define XCB_EVENT_MASK_BUTTON_RELEASE 8)
(define XCB_EVENT_MASK_ENTER_WINDOW 16)
(define XCB_EVENT_MASK_LEAVE_WINDOW 32)
(define XCB_EVENT_MASK_POINTER_MOTION 64)
(define XCB_EVENT_MASK_POINTER_MOTION_HINT 128)
(define XCB_EVENT_MASK_BUTTON_1MOTION 256)
(define XCB_EVENT_MASK_BUTTON_2MOTION 512)
(define XCB_EVENT_MASK_BUTTON_3MOTION 1024)
(define XCB_EVENT_MASK_BUTTON_4MOTION 2048)
(define XCB_EVENT_MASK_BUTTON_5MOTION 4096)
(define XCB_EVENT_MASK_BUTTON_MOTION 8192)
(define XCB_EVENT_MASK_KEYMAP_STATE 16384)
(define XCB_EVENT_MASK_EXPOSURE 32768)
(define XCB_EVENT_MASK_VISIBILITY_CHANGE 65536)
(define XCB_EVENT_MASK_STRUCTURE_NOTIFY 131072)
(define XCB_EVENT_MASK_RESIZE_REDIRECT 262144)
(define XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY 524288)
(define XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT 1048576)
(define XCB_EVENT_MASK_FOCUS_CHANGE 2097152)
(define XCB_EVENT_MASK_PROPERTY_CHANGE 4194304)
(define XCB_EVENT_MASK_COLOR_MAP_CHANGE 8388608)
(define XCB_EVENT_MASK_OWNER_GRAB_BUTTON 16777216)

(define-record-type event
  (fields raw
          synthetic?
          code
          ;; Time of the event in milliseconds (from some unknown
          ;; epoch), or #f. Timestamps wrap around.
          time))

(define-record-type input-event
  (parent event)
  (fields detail root event child root-x root-y event-x event-y state same-screen?))

(define-record-type key-press (parent input-event))
(define-record-type key-release (parent input-event))
(define-record-type button-press (parent input-event))
(define-record-type button-release (parent input-event))
(define-record-type motion-notify (parent input-event))

(define-record-type enter-notify (parent input-event))
(define-record-type leave-notify (parent input-event))

(define-record-type keymap-notify
  (parent event)
  (fields keys))

(define-record-type expose
  (parent event)
  (fields window
          x
          y
          width
          height
          count))

(define-record-type no-exposure
  (parent event)
  (fields drawable
          minor-opcode
          major-opcode))

(define-record-type client-message
  (parent event)
  (fields window
          type                          ;atom
          data))

(define-record-type mapping-notify
  (parent event)
  (fields request                       ;0=Modifier, 1=Keyboard, 2=Mouse
          keycode
          count))

;; XXX: If you decide to handle an event of this type yourself,
;; then use event? and parse event-raw.
(define-record-type unknown-event
  (parent event))

;; Parse an event. These are always 32 bytes long.
(define (parse-xevent bv)
  (let* ((c (unpack "C" bv))
         (code (fxbit-field c 0 7))
         (synthetic (fxbit-set? c 7)))
    (cond
      ((or (eqv? code XCB_KEY_PRESS)
           (eqv? code XCB_KEY_RELEASE)
           (eqv? code XCB_BUTTON_PRESS)
           (eqv? code XCB_BUTTON_RELEASE)
           (eqv? code XCB_MOTION_NOTIFY)
           (eqv? code XCB_ENTER_NOTIFY)
           (eqv? code XCB_LEAVE_NOTIFY))
       (let-values ([(keycode/button time root event child root-x root-y
                                     event-x event-y state same-screen)
                     (unpack "xCxx 4L 4s S Cx" bv)])
         (let ((mk (cond ((eqv? code XCB_KEY_PRESS) make-key-press)
                         ((eqv? code XCB_KEY_RELEASE) make-key-release)
                         ((eqv? code XCB_BUTTON_PRESS) make-button-press)
                         ((eqv? code XCB_MOTION_NOTIFY) make-motion-notify)
                         ((eqv? code XCB_ENTER_NOTIFY) make-enter-notify)
                         ((eqv? code XCB_LEAVE_NOTIFY) make-leave-notify)
                         (else make-button-release))))
           (mk bv synthetic code time
               keycode/button root event child
               root-x root-y event-x event-y state
               (not (eqv? same-screen 0))))))
      ((eqv? code XCB_EXPOSE)
       (let-values ([(window x y w h count) (unpack "4xL5S" bv)])
         (make-expose bv synthetic code #f
                      window x y w h count)))
      ((eqv? code XCB_NO_EXPOSURE)
       (let-values ([(drawable minor major) (unpack "4xLSC" bv)])
         (make-no-exposure bv synthetic code #f drawable minor major)))
      ((eqv? code XCB_CLIENT_MESSAGE)
       (let-values ([(format window type) (unpack "xCxxLL" bv)])
         (let ((data (case format
                       ((32)
                        (list-tail (bytevector->uint-list bv (native-endianness) 4)
                                   (div (format-size "xCxxLL") 4)))
                       ((16)
                        (list-tail (bytevector->uint-list bv (native-endianness) 2)
                                   (div (format-size "xCxxLL") 2)))
                       (else
                        (list-tail (bytevector->u8-list bv)
                                   (format-size "xCxxLL"))))))
           (make-client-message bv synthetic code #f window type data))))
      ((eqv? code XCB_KEYMAP_NOTIFY)
       (make-keymap-notify bv synthetic code #f (cdr (bytevector->u8-list bv))))
      (else
       (make-unknown-event bv synthetic code #f))))))
