;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2008-2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Error handling

(library (yxskaft errors)
  (export
    parse-xerror)
  (import
    (rnrs)
    (struct pack))

(define-condition-type &x-error &error
  make-x-error x-error?
  (sequence-number x-error-sequence-number)
  (major-opcode x-error-major-opcode)
  (minor-opcode x-error-minor-opcode))

(define-condition-type &x-request-error &x-error
  make-x-request-error x-request-error?)

(define-condition-type &x-value-error &x-error
  make-x-value-error x-value-error?)

(define-condition-type &x-window-error &x-error
  make-x-window-error x-window-error?)

(define-condition-type &x-pixmap-error &x-error
  make-x-pixmap-error x-pixmap-error?)

(define-condition-type &x-atom-error &x-error
  make-x-atom-error x-atom-error?)

(define-condition-type &x-cursor-error &x-error
  make-x-cursor-error x-cursor-error?)

(define-condition-type &x-font-error &x-error
  make-x-font-error x-font-error?)

(define-condition-type &x-match-error &x-error
  make-x-match-error x-match-error?)

(define-condition-type &x-drawable-error &x-error
  make-x-drawable-error x-drawable-error?)

(define-condition-type &x-access-error &x-error
  make-x-access-error x-access-error?)

(define-condition-type &x-alloc-error &x-error
  make-x-alloc-error x-alloc-error?)

(define-condition-type &x-colormap-error &x-error
  make-x-colormap-error x-colormap-error?)

(define-condition-type &x-gcontext-error &x-error
  make-x-gcontext-error x-gcontext-error?)

(define-condition-type &x-idchoice-error &x-error
  make-x-idchoice-error x-idchoice-error?)

(define-condition-type &x-name-error &x-error
  make-x-name-error x-name-error?)

(define-condition-type &x-length-error &x-error
  make-x-length-error x-length-error?)

(define-condition-type &x-implementation-error &x-error
  make-x-implementation-error x-implementation-error?)

(define-condition-type &x-unknown-error &x-error
  make-x-unknown-error x-unknown-error?)

;; The errors are in order of code, starting with the invalid code
;; zero that is used when an unknown error code is returned.
(define error-codes
  (vector (list make-x-unknown-error #t "The X server returned an unknown error code")
          (list make-x-request-error #f "An invalid major or minor opcode was sent to the X server")
          (list make-x-value-error #t "An out of range numeric value was sent to the X server")
          (list make-x-window-error #t "An invalid WINDOW was sent to the X server")
          (list make-x-pixmap-error #t "An invalid PIXMAP was sent to the X server")
          (list make-x-atom-error #t "An invalid ATOM was sent to the X server")
          (list make-x-cursor-error #t "An invalid CURSOR was sent to the X server")
          (list make-x-font-error #t "An invalid FONT or FONTABLE (FONT or GCONTEXT) was sent to the X server")
          (list make-x-match-error #f "An invalid combination of arguments was sent in the request to the X server")
          (list make-x-drawable-error #t "An invalid DRAWABLE (WINDOW or PIXMAP) was sent to the X server")
          (list make-x-access-error #f "The X server denied access to the requested resource.")
          (list make-x-alloc-error #f "The X server could not allocate the resource (out of memory?)")
          (list make-x-colormap-error #t "An invalid COLORMAP was sent to the X server")
          (list make-x-gcontext-error #t "An invalid GCONTEXT was sent to the X server")
          (list make-x-idchoice-error #t "A resource ID was sent to the X server that was out of range or already in use")
          (list make-x-name-error #f "A font or color that does not exist was sent to the X server")
          (list make-x-length-error #f "The length of the request sent to the X server was too short or too long")
          (list make-x-implementation-error #f "The X server is deficient and did not fulfill the request")))

(define (parse-xerror bv)
  (let-values ([(code seq-no value minor major) (unpack "xCSLSC" bv)])
    (let ((e (vector-ref error-codes (if (< code (vector-length error-codes))
                                         code 0))))
      (let ((make-the-error (car e))
            (value-valid (cadr e))
            (msg (caddr e)))
        (condition (make-message-condition msg)
                   (make-the-error seq-no major minor)
                   (make-irritants-condition (if value-valid (list value) '()))))))))
