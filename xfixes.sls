;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2022 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; XFIXES extension

;; Just enough to support (yxskaft simple).

(library (yxskaft xfixes)
  (export
    ;; X requests
    xfixes:query-version
    xfixes:show-cursor
    xfixes:hide-cursor

    xfixes:minial-version-for-show/hide

    )
  (import
    (rnrs)
    (struct pack)
    (yxskaft client))

(define extension-name 'XFIXES)

(define XCB_XFIXES_QUERY_VERSION 0)
(define XCB_XFIXES_HIDE_CURSOR 29)
(define XCB_XFIXES_SHOW_CURSOR 30)

(define xfixes:minial-version-for-show/hide 4)

(define (xfixes:query-version c client-major-version client-minor-version)
  (send-request c #f extension-name XCB_XFIXES_QUERY_VERSION
                (pack "LL" client-major-version client-minor-version)))

(define (xfixes:hide-cursor c window)
  (send-request c #f extension-name XCB_XFIXES_HIDE_CURSOR
                (pack "L" (xid-id window))))

(define (xfixes:show-cursor c window)
  (send-request c #f extension-name XCB_XFIXES_SHOW_CURSOR
                (pack "L" (xid-id window)))))
