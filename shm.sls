;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; MIT-SHM

(library (yxskaft shm)
  (export
    ;; X requests
    shm:attach
    shm:create-pixmap
    shm:put-image

    ;; Syscalls
    shmat
    shmctl
    shmdt
    shmget
    IPC_PRIVATE
    IPC_CREAT
    IPC_RMID
    )
  (import
    (rnrs)
    (struct pack)
    (yxskaft client)
    (yxskaft private shm))

(define extension-name 'MIT-SHM)

(define XCB_SHM_QUERY_VERSION 0)
(define XCB_SHM_ATTACH 1)
(define XCB_SHM_DETACH 2)
(define XCB_SHM_PUT_IMAGE 3)
(define XCB_SHM_GET_IMAGE 4)
(define XCB_SHM_CREATE_PIXMAP 5)
(define XCB_SHM_ATTACH_FD 6)
(define XCB_SHM_CREATE_SEGMENT 7)

(define (shm:attach c shmseg shmid read-only)
  (send-request c #f extension-name XCB_SHM_ATTACH
                (pack "LLC" (xid-id shmseg) shmid (if read-only 1 0))))

(define (shm:create-pixmap c pid drawable width height depth shmseg offset)
  (send-request c #f extension-name XCB_SHM_CREATE_PIXMAP
                (pack "LLSSCxxxLL" (xid-id pid) (xid-id drawable)
                      width height depth (xid-id shmseg) offset)))

(define (shm:put-image c drawable gc
                       total-width total-height src-x src-y src-width src-height
                       dst-x dst-y depth format send-event shmseg offset)
  (send-request c #f extension-name XCB_SHM_PUT_IMAGE
                (pack "LL6S2sCCCxLL" (xid-id drawable) (xid-id gc)
                       total-width total-height src-x src-y src-width src-height
                       dst-x dst-y depth format (if send-event 1 0) (xid-id shmseg)
                       offset)))

)
