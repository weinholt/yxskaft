;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020-2021 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Simple library for getting pixels on the screen and reading events

(library (yxskaft simple)
  (export
    create-simple-window
    simple-window-ptr
    simple-window-xconn
    simple-window-depth
    simple-window-bytes-per-line
    render-simple-window
    destroy-simple-window
    simple-window-hide-cursor
    simple-window-show-cursor

    keybutton
    keybutton-set

    simple-window-poll-for-event
    keyboard-event?
    keyboard-event-event
    keyboard-event-bare-keysym
    keyboard-event-modified-keysym
    keyboard-event-modifiers
    keyboard-event-char
    keyboard-event-press?

    mouse-event?
    mouse-event-modifiers
    mouse-event-button
    mouse-event-x
    mouse-event-y
    mouse-event-press?
    mouse-event-release?
    mouse-event-motion?

    delete-window-event?

    sleep     ;FIXME: find a more reliable ticker, maybe the sync extension
    put-pixel32

    XKB_KEY_BackSpace
    XKB_KEY_Tab
    XKB_KEY_Linefeed
    XKB_KEY_Clear
    XKB_KEY_Return
    XKB_KEY_Pause
    XKB_KEY_Scroll_Lock
    XKB_KEY_Sys_Req
    XKB_KEY_Escape
    XKB_KEY_Delete
    XKB_KEY_Home
    XKB_KEY_Left
    XKB_KEY_Up
    XKB_KEY_Right
    XKB_KEY_Down
    XKB_KEY_Prior
    XKB_KEY_Page_Up
    XKB_KEY_Next
    XKB_KEY_Page_Down
    XKB_KEY_End
    XKB_KEY_Begin
    XKB_KEY_KP_Space
    XKB_KEY_KP_Tab
    XKB_KEY_KP_Enter
    XKB_KEY_KP_F1
    XKB_KEY_KP_F2
    XKB_KEY_KP_F3
    XKB_KEY_KP_F4
    XKB_KEY_KP_Home
    XKB_KEY_KP_Left
    XKB_KEY_KP_Up
    XKB_KEY_KP_Right
    XKB_KEY_KP_Down
    XKB_KEY_KP_Prior
    XKB_KEY_KP_Page_Up
    XKB_KEY_KP_Next
    XKB_KEY_KP_Page_Down
    XKB_KEY_KP_End
    XKB_KEY_KP_Begin
    XKB_KEY_KP_Insert
    XKB_KEY_KP_Delete
    XKB_KEY_KP_Equal
    XKB_KEY_KP_Multiply
    XKB_KEY_KP_Add
    XKB_KEY_KP_Separator
    XKB_KEY_KP_Subtract
    XKB_KEY_KP_Decimal
    XKB_KEY_KP_Divide
    XKB_KEY_KP_0
    XKB_KEY_KP_1
    XKB_KEY_KP_2
    XKB_KEY_KP_3
    XKB_KEY_KP_4
    XKB_KEY_KP_5
    XKB_KEY_KP_6
    XKB_KEY_KP_7
    XKB_KEY_KP_8
    XKB_KEY_KP_9
    XKB_KEY_F1
    XKB_KEY_F2
    XKB_KEY_F3
    XKB_KEY_F4
    XKB_KEY_F5
    XKB_KEY_F6
    XKB_KEY_F7
    XKB_KEY_F8
    XKB_KEY_F9
    XKB_KEY_F10
    XKB_KEY_F11
    XKB_KEY_Shift_L
    XKB_KEY_Shift_R
    XKB_KEY_Control_L
    XKB_KEY_Control_R
    XKB_KEY_Caps_Lock
    XKB_KEY_Shift_Lock
    XKB_KEY_Meta_L
    XKB_KEY_Meta_R
    XKB_KEY_Alt_L
    XKB_KEY_Alt_R
    XKB_KEY_Super_L
    XKB_KEY_Super_R
    XKB_KEY_Hyper_L
    XKB_KEY_Hyper_R)
  (import
    (rnrs)
    (yxskaft client)
    (yxskaft events)
    (yxskaft shm)
    (yxskaft xfixes)
    (yxskaft private shm)
    (yxskaft private sleep))

(define-record-type simple-window
  (sealed #t)
  (fields ptr
          xconn
          pid
          win
          gcontext
          depth
          width
          height
          bytes-per-line
          (mutable keyboard-mapping)
          (mutable pointer-mapping)
          (mutable modifier-mapping)
          (mutable xfixes-version)
          (mutable cursor-hidden?)
          (mutable want-cursor-hidden?)))

(define (create-simple-window title width height)
  (define c (open-display #f))
  (define screen (xconn-default-screen c))
  (define win (xconn-generate-xid! c))
  (define gcontext (xconn-generate-xid! c))
  (define shmseg (xconn-generate-xid! c))
  (define pid (xconn-generate-xid! c))

  ;; FIXME: query if the server supports shared-pixmaps

  (create-gc c gcontext (screen-root screen)
             `((foreground . ,(screen-black-pixel screen))
               (graphics-exposures . 0)))
  (create-window c 0 win (screen-root screen)
                 0 0
                 width height
                 10
                 XCB_WINDOW_CLASS_INPUT_OUTPUT
                 (screen-root-visual screen)
                 (list '(background-pixel . #x330000)
                       '(border-pixel . 0)
                       '(bit-gravity . 1)
                       (cons 'event-mask
                             (bitwise-ior XCB_EVENT_MASK_KEY_PRESS
                                          XCB_EVENT_MASK_KEY_RELEASE
                                          XCB_EVENT_MASK_BUTTON_PRESS
                                          XCB_EVENT_MASK_BUTTON_RELEASE
                                          XCB_EVENT_MASK_POINTER_MOTION
                                          XCB_EVENT_MASK_POINTER_MOTION_HINT
                                          XCB_EVENT_MASK_KEYMAP_STATE
                                          XCB_EVENT_MASK_ENTER_WINDOW
                                          XCB_EVENT_MASK_LEAVE_WINDOW))))

  ;; Some very basic ICCCM stuff. Set the title.
  (let ((title (string->utf8 title))
        (title-icon (string->utf8 title)))
    (change-property c XCB_PROP_MODE_REPLACE win
                     'WM_NAME
                     'UTF8_STRING 8
                     (bytevector-length title) title)
    (change-property c XCB_PROP_MODE_REPLACE win
                     'WM_ICON_NAME
                     'UTF8_STRING 8
                     (bytevector-length title-icon) title-icon))

  ;; Say that we will try to honor WM_DELETE_WINDOW.
  (let ((protocols (uint-list->bytevector (list (get-atom-id c 'WM_DELETE_WINDOW))
                                            (native-endianness)
                                            4)))
    (change-property c XCB_PROP_MODE_REPLACE win
                     'WM_PROTOCOLS
                     'ATOM 32
                     (div (bytevector-length protocols) 4)
                     protocols))

  (let* ((depth (screen-root-depth screen))
         (bytes-per-line (* width 4))       ;FIXME: get from server
         (shmid (shmget IPC_PRIVATE (* bytes-per-line height) (fxior IPC_CREAT #o600)))
         (&shmaddr (shmat shmid NULL 0)))
    (guard (exn
            ((serious-condition? exn)
             (shmctl shmid IPC_RMID NULL)
             (raise exn)))
      (check c (shm:attach c shmseg shmid #f))
      (shmctl shmid IPC_RMID NULL)
      (check c (shm:create-pixmap c pid win width height depth shmseg 0))
      (check c (map-window c win))
      (let* ((setup (xconn-setup c))
             (km (get-keyboard-mapping c (setup-min-keycode setup)
                                       (- (setup-max-keycode setup)
                                          (setup-min-keycode setup))))
             (pm (get-pointer-mapping c))
             (mm (get-modifier-mapping c)))
        (make-simple-window &shmaddr
                            c pid win gcontext
                            32 width height bytes-per-line
                            km pm mm #f #f #f)))))

(define (render-simple-window window)
  (copy-area (simple-window-xconn window)
             (simple-window-pid window)
             (simple-window-win window)
             (simple-window-gcontext window)
             0 0
             0 0
             (simple-window-width window)
             (simple-window-height window)))

(define (%negotiate-xfixes window)
  (unless (simple-window-xfixes-version window)
    (let ((c (simple-window-xconn window)))
      (check c (xfixes:query-version c xfixes:minial-version-for-show/hide 0))
      (simple-window-xfixes-version-set! window #t))))

(define (%hide-cursor window hide?)
  ;; The xfixes:hide-cursor request hides the cursor on the whole
  ;; screen for some reason. In addition to this stupidity, it's
  ;; implemented as a counter.
  (unless (boolean=? (simple-window-cursor-hidden? window) hide?)
    (%negotiate-xfixes window)
    (let ((c (simple-window-xconn window))
          (proc (if hide? xfixes:hide-cursor xfixes:show-cursor)))
      (check c (proc c (simple-window-win window)))
      (simple-window-cursor-hidden?-set! window hide?))))

(define (simple-window-hide-cursor window)
  (simple-window-want-cursor-hidden?-set! window #t)
  (%hide-cursor window #t))

(define (simple-window-show-cursor window)
  (simple-window-want-cursor-hidden?-set! window #f)
  (%hide-cursor window #f))

;;; Events

(define-record-type keyboard-event
  (sealed #t)
  (fields event
          bare-keysym
          modified-keysym
          modifiers
          char
          press?))

(define-enumeration keybutton
  (Shift Lock Control
         Mod1 Mod2 Mod3 Mod4 Mod5
         Button1 Button2 Button3 Button4 Button5)
  keybutton-set)

(define-record-type mouse-event
  (fields event
          modifiers                     ;a keybuttons enum
          button                        ;number of the button
          x y))                         ;coordinates in the window

(define-record-type mouse-event-press (parent mouse-event) (sealed #t))
(define-record-type mouse-event-release (parent mouse-event) (sealed #t))
(define-record-type mouse-event-motion (parent mouse-event) (sealed #t))
;; TODO: what about separating out mouse-event-wheel?

(define-record-type delete-window-event
  (sealed #t)
  (fields event))

(define (iota n)
  (do ((n n (fx- n 1))
       (x* '() (cons (fx- n 1) x*)))
      ((eqv? n 0) x*)))

(define (parse-keybutmask mask)
  (let ((all (enum-set->list (enum-set-universe (keybutton-set))))
        (c (enum-set-constructor (keybutton-set))))
    (fold-left enum-set-union
               (keybutton-set)
               (map (lambda (bit name)
                      (if (bitwise-bit-set? mask bit)
                          (c (list name))
                          (keybutton-set)))
                    (iota (length all))
                    all))))

;; See <xkbcommon/xkbcommon-keysyms.h> and especially the comments in
;; that header. There's a gazillion of these.

;; TODO: is there any way to map them back to the actual key that was
;; pressed?

(define XKB_KEY_BackSpace    #xff08)
(define XKB_KEY_Tab          #xff09)
(define XKB_KEY_Linefeed     #xff0a)
(define XKB_KEY_Clear        #xff0b)
(define XKB_KEY_Return       #xff0d)
(define XKB_KEY_Pause        #xff13)
(define XKB_KEY_Scroll_Lock  #xff14)
(define XKB_KEY_Sys_Req      #xff15)
(define XKB_KEY_Escape       #xff1b)
(define XKB_KEY_Delete       #xffff)
(define XKB_KEY_Home         #xff50)
(define XKB_KEY_Left         #xff51)
(define XKB_KEY_Up           #xff52)
(define XKB_KEY_Right        #xff53)
(define XKB_KEY_Down         #xff54)
(define XKB_KEY_Prior        #xff55)
(define XKB_KEY_Page_Up      #xff55)
(define XKB_KEY_Next         #xff56)
(define XKB_KEY_Page_Down    #xff56)
(define XKB_KEY_End          #xff57)
(define XKB_KEY_Begin        #xff58)
(define XKB_KEY_KP_Space     #xff80)
(define XKB_KEY_KP_Tab       #xff89)
(define XKB_KEY_KP_Enter     #xff8d)
(define XKB_KEY_KP_F1        #xff91)
(define XKB_KEY_KP_F2        #xff92)
(define XKB_KEY_KP_F3        #xff93)
(define XKB_KEY_KP_F4        #xff94)
(define XKB_KEY_KP_Home      #xff95)
(define XKB_KEY_KP_Left      #xff96)
(define XKB_KEY_KP_Up        #xff97)
(define XKB_KEY_KP_Right     #xff98)
(define XKB_KEY_KP_Down      #xff99)
(define XKB_KEY_KP_Prior     #xff9a)
(define XKB_KEY_KP_Page_Up   #xff9a)
(define XKB_KEY_KP_Next      #xff9b)
(define XKB_KEY_KP_Page_Down #xff9b)
(define XKB_KEY_KP_End       #xff9c)
(define XKB_KEY_KP_Begin     #xff9d)
(define XKB_KEY_KP_Insert    #xff9e)
(define XKB_KEY_KP_Delete    #xff9f)
(define XKB_KEY_KP_Equal     #xffbd)
(define XKB_KEY_KP_Multiply  #xffaa)
(define XKB_KEY_KP_Add       #xffab)
(define XKB_KEY_KP_Separator #xffac)
(define XKB_KEY_KP_Subtract  #xffad)
(define XKB_KEY_KP_Decimal   #xffae)
(define XKB_KEY_KP_Divide    #xffaf)
(define XKB_KEY_KP_0         #xffb0)
(define XKB_KEY_KP_1         #xffb1)
(define XKB_KEY_KP_2         #xffb2)
(define XKB_KEY_KP_3         #xffb3)
(define XKB_KEY_KP_4         #xffb4)
(define XKB_KEY_KP_5         #xffb5)
(define XKB_KEY_KP_6         #xffb6)
(define XKB_KEY_KP_7         #xffb7)
(define XKB_KEY_KP_8         #xffb8)
(define XKB_KEY_KP_9         #xffb9)
(define XKB_KEY_F1           #xffbe)
(define XKB_KEY_F2           #xffbf)
(define XKB_KEY_F3           #xffc0)
(define XKB_KEY_F4           #xffc1)
(define XKB_KEY_F5           #xffc2)
(define XKB_KEY_F6           #xffc3)
(define XKB_KEY_F7           #xffc4)
(define XKB_KEY_F8           #xffc5)
(define XKB_KEY_F9           #xffc6)
(define XKB_KEY_F10          #xffc7)
(define XKB_KEY_F11          #xffc8)
(define XKB_KEY_Shift_L      #xffe1)
(define XKB_KEY_Shift_R      #xffe2)
(define XKB_KEY_Control_L    #xffe3)
(define XKB_KEY_Control_R    #xffe4)
(define XKB_KEY_Caps_Lock    #xffe5)
(define XKB_KEY_Shift_Lock   #xffe6)
(define XKB_KEY_Meta_L       #xffe7)
(define XKB_KEY_Meta_R       #xffe8)
(define XKB_KEY_Alt_L        #xffe9)
(define XKB_KEY_Alt_R        #xffea)
(define XKB_KEY_Super_L      #xffeb)
(define XKB_KEY_Super_R      #xffec)
(define XKB_KEY_Hyper_L      #xffed)
(define XKB_KEY_Hyper_R      #xffee)

;; Given a window and a keyevent, look up the key symbol and character.
(define (lookup-key window state keycode)
  (define min-keycode (setup-min-keycode (xconn-setup (simple-window-xconn window))))
  (define keyboard-mapping
    (get-keyboard-mapping-reply (simple-window-xconn window)
                                (simple-window-keyboard-mapping window)))
  (define keysyms-per-keycode (vector-ref keyboard-mapping 0))
  (define keysyms (vector-ref keyboard-mapping 1))
  ;; TODO: Implement the full complicated mapping of the state
  (let* ((base (fx* (fx- keycode min-keycode) keysyms-per-keycode))
         (offset (if (eqv? state 0) 0 1))
         (keysym (vector-ref keysyms (fx+ base offset)))
         (char (cond ((<= #x20 keysym #xff)
                      (integer->char keysym))
                     (else
                      #f))))
    ;; (write (number->string keysym 16)) (newline)
    (values keysym keysym char)))

(define (simple-window-poll-for-event window)
  (define c (simple-window-xconn window))
  (let ((e (poll-for-event c)))
    (cond ((not e)
           #f)
          ((or (motion-notify? e) (button-press? e) (button-release? e))
           (let ((keybut (parse-keybutmask (input-event-state e)))
                 (mk (cond ((motion-notify? e) make-mouse-event-motion)
                           ((button-press? e) make-mouse-event-press)
                           (else make-mouse-event-release))))
             (when (and (motion-notify? e) (eqv? XCB_MOTION_HINT (input-event-detail e)))
               ;; Ask the server for more motion events
               (query-pointer c (simple-window-win window)))
             (mk e keybut
                 (input-event-detail e)
                 (input-event-event-x e)
                 (input-event-event-y e))))
          ((or (key-press? e) (key-release? e))
           (let-values ([(bare-keysym modified-keysym char)
                         (lookup-key window (input-event-state e)
                                     (input-event-detail e))])
             (make-keyboard-event e
                                  bare-keysym
                                  modified-keysym
                                  (parse-keybutmask (input-event-state e))
                                  char
                                  (key-press? e))))
          ((mapping-notify? e)
           (case (mapping-notify-request e)
             ((0)
              (simple-window-modifier-mapping-set! window (get-modifier-mapping c)))
             ((1)
              (let ((setup (xconn-setup c)))
                (let ((mapping (get-keyboard-mapping c (setup-min-keycode setup)
                                                     (- (setup-max-keycode setup)
                                                        (setup-min-keycode setup)))))
                  (simple-window-keyboard-mapping-set! window mapping))))
             ((2)
              (simple-window-pointer-mapping-set! window (get-pointer-mapping c))))
           (simple-window-poll-for-event window))
          ((enter-notify? e)
           (when (simple-window-want-cursor-hidden? window)
             (%hide-cursor window #t))
           e)
          ((leave-notify? e)
           (when (simple-window-want-cursor-hidden? window)
             (%hide-cursor window #f))
           e)
          ((client-message? e)
           (cond ((and (eqv? (client-message-type e)
                             (get-atom-id c 'WM_PROTOCOLS))
                       (pair? (client-message-data e))
                       (eqv? (car (client-message-data e))
                             (get-atom-id c 'WM_DELETE_WINDOW)))
                  (make-delete-window-event e))
                 (else
                  (simple-window-poll-for-event window))))
          (else
           (simple-window-poll-for-event window)))))

(define (destroy-simple-window window)
  ;; (shm:detach c shmseg)
  (shmdt (simple-window-ptr window))
  ;; (free-pixmap c pix)
  ;; (destroy-window c win)
  ;; (disconnect c)
  ))
