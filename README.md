# yxskaft - tiny X library for Scheme

> "All problems in computer science can be solved by another level of
> indirection. But that usually will create another problem."
> -- David Wheeler

How hard can it be to change a pixel on the display?

The X Window System is the de facto graphics system for Linux and
BSDs. [Creating a full library for X](https://gitlab.com/weinholt/xsb)
is a big undertaking, which should be based on the work done in XCB.
Such a library, while useful, would be an unwieldy dependency. I've
started, gotten bored and given up a few times.

The yxskaft library has a simpler aim: get pixels on the display and
read keyboard & mouse data. Something almost as simple and fast as
direct access to the framebuffer.

This library implements a subset of the X protocol directly, enough to
get a shared memory pixmap and for reading events. Note that this
requires that the display and the client are on the same machine!

## Current state

The basics are in place, but it's only useful for Loko Scheme users
right now.

* Missing some polish on the put-pixel interface. This interface will
  probably be reworked slightly to check for overflow and to support
  different display depths.
* Works with Loko Scheme 0.7.1 or later. Better use compilation, or
  performance will suffer.
* Kinda works with GNU Guile 3.0.4, but you need to nudge r6rs-pffi to
  load libc.
* Missing compatibility libraries for other Scheme implementations.
  Should maybe just give up, build a small C shared object, and load
  it with r6rs-pffi.
* Missing full decoding of input events. Will be fixed soon. Partial
  decoding of keyboard events is in place.
* Requires MIT-SHM. This might be fixed in a later version of the
  library, but it's not a priority right now. Use VNC if this does not
  work for your use case, or figure out a solution.
* Should probably use the sync extension to get a reliable fps. A
  simple sleep procedure is provided, but it makes for a very obvious
  GC pause.

## License

© 2020 Göran Weinholt

yxskaft is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

yxskaft is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
