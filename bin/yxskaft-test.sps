#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

(import
  (rnrs)
  (yxskaft simple))

;; FIXME: For simplicity, this assumes a 32-bit depth, but it should
;; support 15, 16, 24 and 32.
(define width 320)
(define height 240)
(define window (create-simple-window "yxskaft-test.sps" width height))
(define &pixmap (simple-window-ptr window))
(define depth (simple-window-depth window))
(define bytes-per-line (simple-window-bytes-per-line window))

(define (color-rgb r g b)
  (fxior (fxarithmetic-shift-left (round (* r 255)) 16)
         (fxarithmetic-shift-left (round (* g 255)) 8)
         (fxarithmetic-shift-left (round (* b 255)) 0)))

(define (hsv->rgb H S V)
  (let* ((C (* V S))
         (X (* C (- 1 (abs (- (mod (/ H 60) 2) 1)))))
         (m (- V C)))
    (let-values ([(R* G* B*)
                  (cond ((<= H 60) (values C X 0))
                        ((<= H 120) (values X C 0))
                        ((<= H 180) (values 0 C X))
                        ((<= H 240) (values 0 X C))
                        ((<= H 300) (values X 0 C))
                        (else (values C 0 X)))])
      (color-rgb (+ R* m) (+ G* m) (+ B* m)))))

(define rainbow
  (do ((h 0 (+ h 1))
       (c '() (cons (hsv->rgb h 1 1) c)))
      ((= h 360) (list->vector c))))

(let lp ((i 0))
  (do ((y 0 (fx+ y 1)))
      ((fx=? y height))
    (do ((x 0 (fx+ x 1)))
        ((fx=? x width))
      (put-pixel32 &pixmap
                   (fx+ (fx* y bytes-per-line)
                        (fx* 4 x))
                   (vector-ref rainbow (fxmod (fx+ (fx+ y x) i) 360)))))

  (render-simple-window window)

  (let lp ()
    (let ((e (simple-window-poll-for-event window)))
      (when e
        (display "X event: ")
        (write e)
        (newline)
        (cond ((delete-window-event? e)
               (destroy-simple-window window)
               (exit 0))
              (else
               (lp))))))

  (sleep 1/30)
  (lp (fx+ i 1)))
