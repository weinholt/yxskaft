;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2008-2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Tiny X11 library

;; Should conform to this specification: X Window System Protocol, X
;; Consortium Standard, X Version 11, Release 6.9/7.0.

(library (yxskaft client)
  (export
    open-display
    xconn-default-screen
    xconn-setup

    setup-protocol-major-version
    setup-protocol-minor-version
    setup-release-number
    setup-resource-id-base
    setup-resource-id-mask
    setup-motion-buffer-size
    setup-vendor-len
    setup-maximum-request-length
    setup-roots-len
    setup-pixmap-formats-len
    setup-image-byte-order
    setup-bitmap-format-bit-order
    setup-bitmap-format-scanline-unit
    setup-bitmap-format-scanline-pad
    setup-min-keycode
    setup-max-keycode
    setup-pixmap-formats
    setup-roots

    screen-root
    screen-root-visual
    screen-root-depth
    screen-black-pixel

    xconn-generate-xid!
    xid-id

    check
    send-request
    poll-for-event
    wait-for-event
    flush

    get-atom-id

    create-window
    query-extension
    query-extension-reply
    list-extensions
    list-extensions-reply
    intern-atom
    get-atom-name
    map-window
    change-property
    clear-area
    copy-area
    query-pointer
    create-gc
    get-keyboard-mapping
    get-keyboard-mapping-reply
    get-pointer-mapping
    get-pointer-mapping-reply
    get-modifier-mapping
    get-modifier-mapping-reply

    XCB_IMAGE_ORDER_LSBFIRST
    XCB_IMAGE_ORDER_MSBFIRST

    XCB_WINDOW_CLASS_COPY_FROM_PARENT
    XCB_WINDOW_CLASS_INPUT_OUTPUT
    XCB_WINDOW_CLASS_INPUT_ONLY

    XCB_PROP_MODE_REPLACE
    XCB_PROP_MODE_PREPEND
    XCB_PROP_MODE_APPEND

    XCB_MOTION_NORMAL
    XCB_MOTION_HINT
    )
  (import
    (rnrs)
    (struct pack)
    (yxskaft events)
    (yxskaft errors)
    (yxskaft private concurrency)
    (yxskaft private connect)
    (yxskaft private xauth))

;; The client's protocol version
(define x-major-version 11)
(define x-minor-version 0)

;; Round to next multiple of four
(define (fxround4 x)
  (fxand (fx+ x 3) -4))

;; Constants from xcb

(define XCB_CREATE_WINDOW 1)
(define XCB_MAP_WINDOW 8)
(define XCB_INTERN_ATOM 16)
(define XCB_GET_ATOM_NAME 17)
(define XCB_CHANGE_PROPERTY 18)
(define XCB_QUERY_POINTER 38)
(define XCB_GET_INPUT_FOCUS 43)
(define XCB_CREATE_GC 55)
(define XCB_CLEAR_AREA 61)
(define XCB_COPY_AREA 62)
(define XCB_QUERY_EXTENSION 98)
(define XCB_LIST_EXTENSIONS 99)
(define XCB_GET_KEYBOARD_MAPPING 101)
(define XCB_GET_POINTER_MAPPING 117)
(define XCB_GET_MODIFIER_MAPPING 119)

(define XCB_IMAGE_ORDER_LSBFIRST 0)
(define XCB_IMAGE_ORDER_MSBFIRST 1)
(define XCB_WINDOW_CLASS_COPY_FROM_PARENT 0)
(define XCB_WINDOW_CLASS_INPUT_OUTPUT 1)
(define XCB_WINDOW_CLASS_INPUT_ONLY 2)
(define XCB_PROP_MODE_REPLACE 0)
(define XCB_PROP_MODE_PREPEND 1)
(define XCB_PROP_MODE_APPEND 2)

(define XCB_MOTION_NORMAL 0)
(define XCB_MOTION_HINT 1)

(define-record-type xconn
  (sealed #t)
  (fields comm
          default-screen-number
          setup
          ;; These are data types defined in (yxskaft private
          ;; concurrency).
          extensions
          atom-table
          (mutable next-resource-id))   ;FIXME: concurrency
  (protocol
   (lambda (p)
     (lambda (iport oport default-screen-number setup)
       (p (make-comm iport oport)
          default-screen-number
          setup

          (make-extension-table)
          (make-initial-atom-table)
          (setup-resource-id-base setup))))))

(define-record-type setup
  (sealed #t)
  (opaque #t)
  (fields protocol-major-version
          protocol-minor-version
          release-number
          resource-id-base
          resource-id-mask
          motion-buffer-size
          vendor-len
          maximum-request-length
          roots-len
          pixmap-formats-len
          image-byte-order
          bitmap-format-bit-order
          bitmap-format-scanline-unit
          bitmap-format-scanline-pad
          min-keycode
          max-keycode
          pixmap-formats
          roots))

(define-record-type format
  (sealed #t)
  (fields depth
          bits-per-pixel
          scanline-pad))

(define-record-type screen
  (sealed #t)
  (fields root
          default-colormap
          white-pixel
          black-pixel
          current-input-masks
          width-in-pixels
          height-in-pixels
          width-in-millimeters
          height-in-millimeters
          min-installed-maps
          max-installed-maps
          root-visual
          backing-stores
          save-unders
          root-depth
          allowed-depths))

(define-record-type depth
  (sealed #t)
  (fields depth visualtypes))

(define-record-type visualtype
  (sealed #t)
  (fields visual-id
          class
          bits-per-rgb-value
          colormap-entries
          red-mask
          green-mask
          blue-mask))

(define-record-type xid
  (sealed #t)
  (fields id))

(define (xconn-generate-xid! c)
  ;; FIXME: This should support the case where id-mask is
  ;; non-contiguous.
  (let ((id (xconn-next-resource-id c)))
    (xconn-next-resource-id-set! c (+ id 1))
    (make-xid (bitwise-ior (setup-resource-id-base (xconn-setup c)) id))))

(define (make-initial-atom-table)
  ;; Atom IDs for these are from 1 to 68 inclusive
  (define atoms
    '(PRIMARY
      SECONDARY ARC ATOM BITMAP CARDINAL COLORMAP CURSOR
      CUT_BUFFER0 CUT_BUFFER1 CUT_BUFFER2 CUT_BUFFER3 CUT_BUFFER4
      CUT_BUFFER5 CUT_BUFFER6 CUT_BUFFER7 DRAWABLE FONT INTEGER PIXMAP POINT
      RECTANGLE RESOURCE_MANAGER RGB_COLOR_MAP RGB_BEST_MAP RGB_BLUE_MAP
      RGB_DEFAULT_MAP RGB_GRAY_MAP RGB_GREEN_MAP RGB_RED_MAP STRING VISUALID
      WINDOW WM_COMMAND WM_HINTS WM_CLIENT_MACHINE WM_ICON_NAME WM_ICON_SIZE
      WM_NAME WM_NORMAL_HINTS WM_SIZE_HINTS WM_ZOOM_HINTS MIN_SPACE
      NORM_SPACE MAX_SPACE END_SPACE SUPERSCRIPT_X SUPERSCRIPT_Y SUBSCRIPT_X
      SUBSCRIPT_Y UNDERLINE_POSITION UNDERLINE_THICKNESS STRIKEOUT_ASCENT
      STRIKEOUT_DESCENT ITALIC_ANGLE X_HEIGHT QUAD_WIDTH WEIGHT POINT_SIZE
      RESOLUTION COPYRIGHT NOTICE FONT_NAME FAMILY_NAME FULL_NAME CAP_HEIGHT
      WM_CLASS WM_TRANSIENT_FOR))
  (do ((ht (make-atom-table))
       (a atoms (cdr a))
       (i 1 (+ i 1)))
      ((null? a) ht)
    (atom-table-set! ht (car a) i)))

;;; Input/output encoding

(define (put-padding port unpadded-len)
  (let ((padding (fx- (fxround4 unpadded-len) unpadded-len)))
    (do ((i 0 (fx+ i 1)))
        ((fx=? i padding))
      (put-u8 port 0))))

(define (put-string8 port s)
  (cond ((string? s)
         (put-string8 port (string->utf8 s)))
        (else
         (put-bytevector port s)
         (put-padding port (bytevector-length s)))))

(define (get-string8 port len)
  (if (eqv? len 0)
      #vu8()
      (let ((bv (get-bytevector-n port len)))
        (get-bytevector-n port (fx- (fxround4 len) len))
        bv)))

(define (encode-value-list values names)
  (call-with-bytevector-output-port
    (lambda (p)
      (let lp ((mask 0)
               (i 0)
               (longs '())
               (names names))
        (cond ((null? names)
               (put-pack p "L" mask)
               (for-each (lambda (v)
                           (put-pack p "L" (bitwise-and #xffffffff v)))
                         (reverse longs)))
              ((assq (car names) values) =>
               (lambda (v)
                 (lp (bitwise-ior mask (bitwise-arithmetic-shift-left 1 i))
                     (fx+ i 1)
                     (cons (cdr v) longs)
                     (cdr names))))
              (else
               (lp mask (fx+ i 1) longs (cdr names))))))))

;;; Connect to an X server

(define (open-display display-string)
  (define (do-n n proc)
    (let lp ((i n))
      (if (eqv? i 0)
          '()
          (cons (proc) (lp (fx- i 1))))))
  (let-values ([(i o auth screen-number) (open-display* display-string)])
    (put-pack o "CSSSS2x"
              (case (native-endianness)
                ((little) (char->integer #\l))
                ((big) (char->integer #\B))
                (else (error 'open-display "Unsupported native endianness"
                             (native-endianness))))
              x-major-version x-minor-version
              (string-length (xauth-name auth))
              (bytevector-length (xauth-data auth)))
    (put-string8 o (xauth-name auth))
    (put-string8 o (xauth-data auth))
    (flush-output-port o)
    (let-values ([(status reason-length protocol-major-version protocol-minor-version
                          additional-data-len)
                  (get-unpack i "CCSSS")])
      (let* ((data (get-bytevector-n i (fx* additional-data-len 4)))
             (p (open-bytevector-input-port data))
             (reason (utf8->string (get-string8 p reason-length))))
        ;; Handle errors
        (case status
          ((1) #f)
          ((0)
           (close-port o)
           (error 'open-display "The display server rejected the connection"
                  display-string reason))
          ((2)
           (close-port o)
           (error 'open-display "The display server requires further authorization"
                  display-string reason))
          (else
           (close-port o)
           (error 'open-display "Unknown status from the display server"
                  display-string status reason)))
        ;; Success
        (let-values ([(release-number
                       resource-id-base resource-id-mask motion-buffer-size
                       vendor-len maximum-request-length roots-len
                       pixmap-formats-len image-byte-order bitmap-format-bit-order
                       bitmap-format-scanline-unit bitmap-format-scanline-pad
                       min-keycode max-keycode)
                      (get-unpack p "=4L 2S 8C 4x")])
          (let* ((vendor (utf8->string (get-string8 p vendor-len)))
                 (pixmap-formats (do-n pixmap-formats-len
                                       (lambda ()
                                         (let-values ([(depth bits-per-pixel scanline-pad)
                                                       (get-unpack p "3C5x")])
                                           (make-format depth bits-per-pixel scanline-pad)))))
                 (roots (do-n roots-len
                              (lambda ()
                                (let-values ([(root default-colormap white-pixel black-pixel
                                                    current-input-masks width-in-pixels height-in-pixels
                                                    width-in-millimeters height-in-millimeters
                                                    min-installed-maps max-installed-maps root-visual
                                                    backing-stores save-unders root-depth
                                                    allowed-depths-len)
                                              (get-unpack p "5L 6S L 4C")])
                                  (let ((allowed-depths
                                         (do-n allowed-depths-len
                                               (lambda ()
                                                 (let-values ([(depth visuals-len) (get-unpack p "CS4x")])
                                                   (let ((visualtypes
                                                          (do-n visuals-len
                                                                (lambda ()
                                                                  (let-values ([x (get-unpack p "LCCS3L4x")])
                                                                    (apply make-visualtype x))))))
                                                     (make-depth depth visualtypes)))))))
                                    (make-screen (make-xid root) (make-xid default-colormap)
                                                 white-pixel black-pixel
                                                 current-input-masks width-in-pixels height-in-pixels
                                                 width-in-millimeters height-in-millimeters
                                                 min-installed-maps max-installed-maps root-visual
                                                 backing-stores (not (eqv? 0 save-unders)) root-depth
                                                 allowed-depths)))))))
            (let ((setup (make-setup protocol-major-version protocol-minor-version release-number
                                     resource-id-base resource-id-mask motion-buffer-size
                                     vendor-len maximum-request-length roots-len
                                     pixmap-formats-len image-byte-order bitmap-format-bit-order
                                     bitmap-format-scanline-unit bitmap-format-scanline-pad
                                     min-keycode max-keycode
                                     pixmap-formats roots)))
              (make-xconn i o screen-number setup))))))))

(define (xconn-default-screen c)
  (list-ref (setup-roots (xconn-setup c))
            (xconn-default-screen-number c)))

(define (xconn-get-extension c name)
  (cond ((extension-table-ref (xconn-extensions c) name) =>
         (lambda (ext-data)
           (vector-ref ext-data 0)))
        (else
         (let* ((name^ (string->utf8 (symbol->string name)))
                (cookie (query-extension c (bytevector-length name^) name^))
                (ext-data (query-extension-reply c cookie)))
           (when (not ext-data)
             (error 'xconn-get-extension
                    "The display server does not have this extension" c name))
           (extension-table-set! (xconn-extensions c) name ext-data)
           (vector-ref ext-data 0)))))

;; Queue a request for transmission and return a cookie.
(define (send-request c has-reply? major-opcode minor-opcode . data*)
  (let ((major-opcode (if (symbol? major-opcode)
                          (xconn-get-extension c major-opcode)
                          major-opcode)))
    (comm-send-request (xconn-comm c) has-reply? major-opcode minor-opcode data*)))

(define (wait-for-event c)
  (flush c)
  (comm-wait-for-event (xconn-comm c)))

(define (poll-for-event c)
  (flush c)
  (comm-poll-for-event (xconn-comm c)))

;; Raise an exception of the cookie contains an error. Used to check
;; errors from requests.
(define (check c cookie)
  (let ((v (cookie-value cookie)))
    (when (not v)
      (flush c)
      (cookie-wait cookie))
    (let ((v (cookie-value cookie)))
      (when (condition? v)
        (raise v))
      v)))

(define (cookie-value/raise who c cookie)
  (let ((v (cookie-value cookie)))
    (when (not v)
      (flush c)
      (cookie-wait cookie))
    (let ((v (cookie-value cookie)))
      (when (condition? v)
        (raise (condition (make-who-condition who) v)))
      v)))

;; Force the output buffer to empty.
(define (flush c)
  (comm-flush (xconn-comm c)))

;;; Requests

(define (create-window c depth wid parent x y width height
                       border-width class visual values)
  (send-request c #f XCB_CREATE_WINDOW depth
                (pack "LL ss 4S L" (xid-id wid) (xid-id parent)
                      x y width height border-width class visual)
                (encode-value-list values
                                   '(background-pixmap
                                     background-pixel border-pixmap border-pixel
                                     bit-gravity win-gravity
                                     backing-store backing-planes backing-pixel
                                     override-redirect save-under event-mask
                                     do-not-propagate-mask colormap cursor))))

(define (map-window c wid)
  (send-request c #f XCB_MAP_WINDOW 0 (pack "L" (xid-id wid))))

(define (intern-atom c only-if-exists name-len name)
  (send-request c #f XCB_INTERN_ATOM (if only-if-exists 1 0)
                (pack "Sxx" name-len)
                name))

(define (intern-atom-reply c cookie)
  (let ((id (unpack "8xL" (cookie-value/raise 'intern-atom-reply c cookie))))
    (if (eqv? id 0)
        #f
        id)))

(define (get-atom-name c only-if-exists atom)
  (send-request c #f XCB_GET_ATOM_NAME 0
                (pack "L" atom)))

(define (get-atom-id c name)
  (cond ((fixnum? name) name)
        ((atom-table-ref (xconn-atom-table c) name))
        (else
         (let* ((name^ (string->utf8 (symbol->string name)))
                (cookie (intern-atom c #f (bytevector-length name^) name^)))
           (let ((id (intern-atom-reply c cookie)))
             (atom-table-set! (xconn-atom-table c) name id)
             id)))))

(define (change-property c mode window property type format data-len data)
  (send-request c #f XCB_CHANGE_PROPERTY mode
                (pack "LLLCL" (xid-id window) (get-atom-id c property) (get-atom-id c type)
                      format data-len)
                data))

(define (clear-area c exposures window x y width height)
  (send-request c #f XCB_CLEAR_AREA (if exposures 1 0)
                (pack "L4S" (xid-id window) x y width height)))

(define (copy-area c src-drawable dst-drawable gc src-x
                   src-y dst-x dst-y width height)
  (send-request c #f XCB_COPY_AREA 0
                (pack "LLL4s2S" (xid-id src-drawable) (xid-id dst-drawable)
                      (xid-id gc) src-x src-y dst-x dst-y width height)))

(define (query-pointer c window)
  (send-request c #f XCB_QUERY_POINTER 0
                (pack "L" (xid-id window))))

(define (create-gc c cid drawable values)
  (send-request c #f XCB_CREATE_GC 0
                (pack "LL" (xid-id cid) (xid-id drawable))
                (encode-value-list values
                                   '(function
                                     plane-mask foreground background
                                     line-width line-style cap-style
                                     join-style fill-style fill-rule
                                     tile stipple tile-stipple-x-origin
                                     tile-stipple-y-origin font
                                     subwindow-mode graphics-exposures
                                     clip-x-origin clip-y-origin clip-mask
                                     dash-offset dashes arc-mode))))

(define (query-extension c name-len name)
  (send-request c #t XCB_QUERY_EXTENSION 0
                (pack "Sxx" name-len)
                name))

(define (query-extension-reply c cookie)
  (let ((bv (cookie-value/raise 'query-extension-reply c cookie)))
    (let-values ([(present major-opcode first-event first-error) (unpack "8x4C" bv)])
      (and (eqv? present 1)
           (vector major-opcode first-event first-error)))))

(define (list-extensions c)
  (send-request c #t XCB_LIST_EXTENSIONS 0))

(define (list-extensions-reply c cookie)
  (call-with-port (open-bytevector-input-port
                   (cookie-value/raise 'list-extensions-reply c cookie))
    (lambda (p)
      (let lp ((n (get-unpack p "xC6x24x"))
               (exts '()))
        (if (eqv? n 0)
            (reverse exts)
            (let* ((len (get-u8 p))
                   (str (utf8->string (get-bytevector-n p len))))
              (lp (- n 1) (cons str exts))))))))

(define (get-keyboard-mapping c first-keycode count)
  (send-request c #t XCB_GET_KEYBOARD_MAPPING 0
                (pack "CC" first-keycode count)))

(define (get-keyboard-mapping-reply c cookie)
  (call-with-port (open-bytevector-input-port
                   (cookie-value/raise 'get-keyboard-mapping-reply c cookie))
    (lambda (p)
      (let ((keysyms-per-keycode (get-unpack p "xC6x24x")))
        (let lp ((keysyms '()))
          (if (port-eof? p)
              (vector keysyms-per-keycode (list->vector (reverse keysyms)))
              (let ((keysym (get-unpack p "L")))
                (lp (cons keysym keysyms)))))))))

(define (get-pointer-mapping c)
  (send-request c #t XCB_GET_POINTER_MAPPING 0))

(define (get-pointer-mapping-reply c cookie)
  (call-with-port (open-bytevector-input-port
                   (cookie-value/raise 'get-pointer-mapping-reply c cookie))
    (lambda (p)
      (let lp ((n (get-unpack p "xC6x24x"))
               (mappings '()))
        (if (eqv? n 0)
            (list->vector (reverse mappings))
            (lp (fx- n 1) (cons (get-u8 p) mappings)))))))

(define (get-modifier-mapping c)
  (send-request c #t XCB_GET_MODIFIER_MAPPING 0))

(define (get-modifier-mapping-reply c cookie)
  (call-with-port (open-bytevector-input-port
                   (cookie-value/raise 'get-modifier-mapping-reply c cookie))
    (lambda (p)
      (let ((keycodes-per-modifier (get-unpack p "xC6x24x")))
        (let lp ((n (fx* 8 keycodes-per-modifier))
                 (keycodes '()))
          (if (eqv? n 0)
              (list->vector (reverse keycodes))
              (lp (fx- n 1) (cons (get-u8 p) keycodes)))))))))
