;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

(library (yxskaft private shm)
  (export
    (rename (sys_shmat shmat)
            (sys_shmctl shmctl)
            (sys_shmdt shmdt)
            (sys_shmget shmget))
    IPC_PRIVATE
    IPC_CREAT
    IPC_RMID
    NULL
    put-pixel32
    )
  (import
    (rnrs)
    (loko system unsafe)
    (loko arch amd64 linux-numbers)
    (loko arch amd64 linux-syscalls))

(define NULL 0)

(define-syntax put-pixel32
  (lambda (x)
    (syntax-case x ()
      [(_ &pixmap offset color)
       #'(put-mem-u32 (fx+ &pixmap offset) color)])))

)
