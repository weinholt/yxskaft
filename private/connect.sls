;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2008-2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Establish a display server connection

(library (yxskaft private connect)
  (export
    open-display*)
  (import
    (rnrs)
    (srfi :98 os-environment-variables)
    (yxskaft private local)               ;Unix domain sockets
    (yxskaft private xauth))

(define (string-split str c)
  (let lp ((start 0) (end 0))
    (cond ((fx=? end (string-length str))
           (list (substring str start end)))
          ((char=? c (string-ref str end))
           (cons (substring str start end)
                 (lp (fx+ end 1) (fx+ end 1))))
          (else
           (lp start (fx+ end 1))))))

;; The syntax for DISPLAY: hostname:displaynumber.screennumber
;; hostname:number   TCP to hostname port 6000+number
;; :number           the fastest mechanism (e.g. /tmp/.X11-unix/X0)
;; hostname::number  DECnet
;; The .screennumber part can be omitted.

;; Parse a DISPLAY string and return three values: connection method
;; (decnet, local or tcp), service or file name, and default screen
;; number.
(define (parse-display display)
  (define (pdisplay s)
    (car (string-split s #\.)))
  (define (pscreen s)
    (let ((s (string-split s #\.)))
      (if (null? (cdr s))
          0
          (string->number (cadr s)))))
  (define (parse-ipv6 display)
    (let* ((d (string-split (substring display 1 (string-length display)) #\]))
           (addr (car d))
           (disp (substring (cadr d) 1 (string-length (cadr d)))))
      (values 'tcp addr
              (number->string (+ 6000 (string->number (pdisplay disp))))
              (pscreen disp))))
  (let ((d (string-split display #\:)))
    (cond ((and (>= (string-length display) 7)
                (char=? #\[ (string-ref display 0)))
           (parse-ipv6 display))
          ((= (length d) 3)
           ;; Not really supported, it's here for completeness.
           (values 'decnet (car d)
                   (pdisplay (caddr d))
                   (pscreen (caddr d))))
          ((not (= (length d) 2))
           (error 'parse-display "Can't parse this DISPLAY string" display))
          ((string=? (car d) "")
           (values 'Local
                   (string-append "/tmp/.X11-unix/X" (pdisplay (cadr d)))
                   #f
                   (pscreen (cadr d))))
          (else
           (values 'TCP/IP (car d)
                   (number->string (+ 6000 (string->number (pdisplay (cadr d)))))
                   (pscreen (cadr d)))))))

(define (find-authority auths method name service)
  ;; TODO: Pick the right authority record. This is only matching
  ;; protocols with address families, but it should match on the
  ;; hostname. For local, does it need to know the system's hostname?
  (find (case method
          ((Local)
           (lambda (auth)
             (eq? (xauth-family auth) 'Local)))
          ((TCP/IP)
           (lambda (auth)
             (memq (xauth-family auth) '(Internet Internet6))))
          (else
           (lambda (auth)
             #t)))
        auths))

;; With the value of a DISPLAY variable (e.g. ":0.0") or #f, find an
;; appropriate authority record and open a connection to the X server.
;; Returns binary input and output ports, the xauth record and the
;; screen number.
(define (open-display* display)
  (let ((display (or display (get-environment-variable "DISPLAY"))))
    (unless (string? display)
      (error 'open-display
             "No display given (try the environment variable DISPLAY)"))
    (let-values ([(method name service screen) (parse-display display)])
      (let* ((auths (xauth-read-file (xauth-find-file)))
             (auth (find-authority auths method name service)))
        (let-values ([(i o) (case method
                              ((Local) (local-connect name))
                              #;((TCP/IP) (tcp-connect name service))
                              (else
                               (error 'open-display* "Unimplemented method"
                                      display method name service screen)))])
          (values i o auth screen)))))))
