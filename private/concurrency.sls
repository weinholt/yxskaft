;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Handle X server communication (fallback edition)

;; XXX: This library is a fallback for when the Scheme implementation
;; does not have any concurrency. A consequence is that latency hiding
;; is not done.

(library (yxskaft private concurrency)
  (export
    make-extension-table
    extension-table-set!
    extension-table-ref

    make-atom-table
    atom-table-set!
    atom-table-ref

    cookie? cookie-seqno cookie-value cookie-wait

    make-comm comm?
    comm-poll-for-event
    comm-wait-for-event
    comm-send-request
    comm-flush)
  (import
    (rnrs)
    (rnrs mutable-pairs)
    (yxskaft events)
    (yxskaft private wire))

;; This queue code is based on public domain code from SLIB,
;; originally written by Andrew Wilcox in 1992. Here it has been
;; reduced in size and rewritten to use mutable pairs.

(define (make-queue)
  (cons '() '()))

(define (enqueue! q datum)
  (let ((new-pair (cons datum '())))
    (if (null? (car q))
        (set-car! q new-pair)
        (set-cdr! (cdr q) new-pair))
    (set-cdr! q new-pair)))

(define (dequeue! q)
  (let ((first-pair (car q)))
    (if (null? first-pair)
        (error 'dequeue! "attempt to dequeue an empty queue"))
    (let ((first-cdr (cdr first-pair)))
      (set-car! q first-cdr)
      (when (null? first-cdr)
        (set-cdr! q '()))
      (car first-pair))))

(define (queue-empty? q)
  (null? (car q)))

(define (queue-front q)
  (caar q))

;; Map from e.g. 'MIT-SHM to #(opcode first-event first-error)
(define make-extension-table make-eq-hashtable)
(define extension-table-set! hashtable-set!)
(define (extension-table-ref table sym)
  (hashtable-ref table sym #f))

;; Map from e.g. 'PRIMARY to 1
(define make-atom-table make-eq-hashtable)
(define atom-table-set! hashtable-set!)
(define (atom-table-ref table sym)
  (hashtable-ref table sym #f))

(define XCB_GET_INPUT_FOCUS 43)

(define-record-type cookie
  (sealed #t)
  (fields seqno comm (mutable value)))

(define (cookie-wait cookie)
  (cond ((cookie-value cookie))
        (else
         (comm-handle-input (cookie-comm cookie))
         (cookie-wait cookie))))

(define-record-type comm
  (fields iport
          oport
          (mutable seqno)
          (mutable seqno-in)
          eventq
          cookieq
          (mutable expecting-reply))
  (protocol
   (lambda (p)
     (lambda (iport oport)
       (let* ((eventq (make-queue))
              (cookieq (make-queue))
              (expecting-reply #f))
         (p iport oport 1 0 eventq cookieq expecting-reply))))))

(define (comm-handle-input comm)
  (define q (comm-cookieq comm))

  (unless (comm-expecting-reply comm)
    ;; Provoke a reply from the server. Unless the reply has arrived,
    ;; reads will not block indefinitely. This has to be done unless
    ;; the client already sent something that will provoke a reply.
    ;; This is, naturally, very bad for latency hiding. See the Loko
    ;; version of this library, it properly hides latency.
    (comm-send-request comm #t XCB_GET_INPUT_FOCUS 0 '())
    (comm-flush comm))

  (let-values ([(seqno msg) (read-message-raw (comm-iport comm) (comm-seqno-in comm))])
    (comm-seqno-in-set! comm seqno)
    (when (event? msg)
      (enqueue! (comm-eventq comm) msg))
    (let lpq ()
      (unless (queue-empty? q)
        (let ((cookie (queue-front q)))
          (cond ((= seqno (cookie-seqno cookie))
                 (cookie-value-set! cookie msg)
                 (dequeue! q))
                ((> seqno (cookie-seqno cookie))
                 (cookie-value-set! cookie 'no-data)
                 (dequeue! q)
                 (lpq))))))

    (if (and (comm-expecting-reply comm)
             (not (>= seqno (comm-expecting-reply comm))))
        (comm-handle-input comm)
        (comm-expecting-reply-set! comm #f))))

(define (get-sequence-number! comm)
  (let ((number (comm-seqno comm)))
    (comm-seqno-set! comm (+ number 1))
    number))

(define (comm-poll-for-event comm)
  (cond ((queue-empty? (comm-eventq comm))
         (comm-handle-input comm)
         (if (queue-empty? (comm-eventq comm))
             #f
             (dequeue! (comm-eventq comm))))
        (else
         (dequeue! (comm-eventq comm)))))

(define (comm-wait-for-event comm)
  (cond ((queue-empty? (comm-eventq comm))
         (comm-handle-input comm)
         (comm-wait-for-event comm))
        (else
         (dequeue! (comm-eventq comm)))))

(define (comm-send-request comm has-reply? major-opcode minor-opcode data*)
  (let* ((seqno (get-sequence-number! comm))
         (cookie (make-cookie seqno comm #f)))
    (send-request-raw (comm-oport comm) major-opcode minor-opcode data*)
    (when has-reply?
      (comm-expecting-reply-set! comm seqno))
    (enqueue! (comm-cookieq comm) cookie)
    cookie))

(define (comm-flush comm)
  (flush-output-port (comm-oport comm))))
