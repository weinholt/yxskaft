;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; X11 wire protocol

(library (yxskaft private wire)
  (export
    send-request-raw
    read-message-raw)
  (import
    (rnrs)
    (struct pack)
    (yxskaft events)
    (yxskaft errors))

;; Round to next multiple of four
(define (fxround4 x)
  (fxand (fx+ x 3) -4))

;; Send an X request to the server. Returns the sequence number of the
;; request.
(define (send-request-raw oport major-opcode minor-opcode data*)
  (let* ((len (apply + (map bytevector-length data*)))
         (len^ (fxround4 len)))
    ;; TODO: BIG-REQUEST
    (put-bytevector oport (pack "CCS" major-opcode minor-opcode
                                (+ 1 (fxdiv len^ 4))))
    (for-each (lambda (bv) (put-bytevector oport bv)) data*)
    (put-bytevector oport #vu8(0 0 0 0) 0 (fx- len^ len))))

;; Read an error, reply or event from the connection. Errors and
;; events are returned parsed; replies are returned as bytevectors.
;; Returns the sequence number and the message.
(define (read-message-raw iport previous-seqno)
  (let ((bv0 (get-bytevector-n iport 32)))
    (let-values ([(code _detail seq-no rep-len) (unpack "CCSL" bv0)])
      (let ((seq-no (if (eqv? code 11)
                        previous-seqno ;XXX: KeymapNotify has no seq-no
                        (let ((seqno (bitwise-ior (bitwise-and previous-seqno (fxnot #xFFFF))
                                                  seq-no)))
                          (if (< seqno previous-seqno)
                              (+ seqno #x10000)
                              seqno)))))
        (case code
          ((0)                          ;error
           (values seq-no (parse-xerror bv0)))
          ((1)                          ;reply
           (let* ((data-len (fx* rep-len 4))
                  (msg (make-bytevector (fx+ 32 data-len))))
             (bytevector-copy! bv0 0 msg 0 32)
             (let ((n (get-bytevector-n! iport msg 32 data-len)))
               (unless (eqv? data-len n)
                 (error 'read-message-raw "Short read" iport n data-len)))
             (values seq-no msg)))
          (else                         ;event
           (values seq-no (parse-xevent bv0)))))))))
