;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Handle X server communication (Loko Scheme edition)

;; XXX: Some of this only works because Loko uses cooperative
;; scheduling between fibers in the same thread.

(library (yxskaft private concurrency)
  (export
    make-extension-table
    extension-table-set!
    extension-table-ref

    make-atom-table
    atom-table-set!
    atom-table-ref

    cookie? cookie-seqno cookie-value cookie-wait

    make-comm comm?
    comm-poll-for-event
    comm-wait-for-event
    comm-send-request
    comm-flush)
  (import
    (rnrs)
    (yxskaft events)
    (yxskaft private wire)
    (loko match)
    (loko queues)
    (loko system fibers))

;; Map from e.g. 'MIT-SHM to #(opcode first-event first-error)
(define make-extension-table make-eq-hashtable)
(define extension-table-set! hashtable-set!)
(define (extension-table-ref table sym)
  (hashtable-ref table sym #f))

;; Map from e.g. 'PRIMARY to 1
(define make-atom-table make-eq-hashtable)
(define atom-table-set! hashtable-set!)
(define (atom-table-ref table sym)
  (hashtable-ref table sym #f))

(define XCB_GET_INPUT_FOCUS 43)

(define-record-type cookie
  (sealed #t)
  (fields seqno (mutable cvar) (mutable value)))

(define (cookie-wait cookie)
  (wait (cookie-cvar cookie)))

(define-record-type comm
  (fields iport
          oport
          (mutable seqno)
          out-ch
          eventq
          (mutable event-cvar))
  (protocol
   (lambda (p)
     (lambda (iport oport)
       (let* ((out-ch (make-channel))
              (eventq (make-queue))
              (comm (p iport oport 1 out-ch eventq (make-cvar))))
         (define q (make-queue))
         (spawn-fiber
          (lambda ()
            (let lp ((expecting-reply #f))
              (match (get-message out-ch)
                [#(cookie has-reply? major-opcode minor-opcode data*)
                 (send-request-raw oport major-opcode minor-opcode data*)
                 (enqueue! q cookie)
                 (lp has-reply?)]
                ['flush
                 ;; Send GetInputFocus to get a response. Otherwise we
                 ;; will not get any response to requests that succeed
                 ;; but that don't have their own reply.
                 (unless expecting-reply
                   (get-sequence-number! comm)
                   (send-request-raw oport XCB_GET_INPUT_FOCUS 0 '()))
                 (flush-output-port oport)
                 (lp #t)]))))
         (spawn-fiber
          (lambda ()
            (let lp ((seqno 0))
              (let-values ([(seqno msg) (read-message-raw iport seqno)])
                (when (event? msg)
                  (enqueue! eventq msg)
                  ;; Wake up comm-wait-for-event
                  (let ((prev-cvar (comm-event-cvar comm)))
                    (comm-event-cvar-set! comm (make-cvar))
                    (signal-cvar! prev-cvar)))
                (let lpq ()
                  (unless (queue-empty? q)
                    (let ((cookie (queue-front q)))
                      (cond ((= seqno (cookie-seqno cookie))
                             (cookie-value-set! cookie msg)
                             (signal-cvar! (cookie-cvar cookie))
                             (dequeue! q))
                            ((> seqno (cookie-seqno cookie))
                             (cookie-value-set! cookie 'no-data)
                             (signal-cvar! (cookie-cvar cookie))
                             (dequeue! q)
                             (lpq))))))
                (lp seqno)))))
         comm)))))

(define (get-sequence-number! comm)
  (let ((number (comm-seqno comm)))
    (comm-seqno-set! comm (+ number 1))
    number))

(define (comm-poll-for-event comm)
  (yield-current-task)
  (if (queue-empty? (comm-eventq comm))
      #f
      (dequeue! (comm-eventq comm))))

(define (comm-wait-for-event comm)
  (cond ((queue-empty? (comm-eventq comm))
         (wait (comm-event-cvar comm))
         (comm-wait-for-event comm))
        (else
         (dequeue! (comm-eventq comm)))))

(define (comm-send-request comm has-reply? major-opcode minor-opcode data*)
  (let* ((seqno (get-sequence-number! comm))
         (cookie (make-cookie seqno (make-cvar) #f)))
    (put-message (comm-out-ch comm)
                 (vector cookie has-reply? major-opcode minor-opcode data*))
    cookie))

(define (comm-flush comm)
  (put-message (comm-out-ch comm) 'flush)))
