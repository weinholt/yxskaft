;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;; TODO: Support other targets!

;; TODO: Make this common with the one in AC/D-Bus

(library (yxskaft private local)
  (export
    local-connect)
  (import
    (rnrs (6))
    (only (loko) port-file-descriptor-set!)
    (loko system fibers)
    (loko system unsafe)
    (loko arch amd64 linux-syscalls)
    (loko arch amd64 linux-numbers))

(define (filename->c-string who fn)
  (unless (string? fn)
    (assertion-violation who "Expected a string" fn))
  (string-for-each
   (lambda (c)
     (when (eqv? c #\nul)
       ;; The filename is not representable on Linux
       (raise (condition
               (make-who-condition who)
               (make-i/o-filename-error fn)
               (make-message-condition "Unrepresentable filename")
               (make-irritants-condition (list fn))))))
   fn)
  (string->utf8 (string-append fn "\x0;")))

(define (local-connect filename)
  (define NULL 0)
  (define fd
    (sys_socket AF_LOCAL (fxior SOCK_STREAM SOCK_CLOEXEC SOCK_NONBLOCK) 0))
  (define (sendto fd buf start count)
    (define flags 0)
    (assert (fx<=? 0 (fx+ start count) (bytevector-length buf)))
    (sys_sendto fd (fx+ (bytevector-address buf) start) count
                flags NULL 0
                (lambda (errno)
                  (cond ((eqv? errno EAGAIN)
                         (wait-for-writable fd)
                         (sendto fd buf start count))
                        ((eqv? errno EINTR)
                         (sendto fd buf start count))
                        (else
                         (raise (condition
                                 (make-syscall-error 'sendto errno)
                                 (make-irritants-condition (list fd)))))))))
  (define (recvfrom fd buf start count)
    (assert (fx<=? 0 (fx+ start count) (bytevector-length buf)))
    (sys_recvfrom fd (fx+ start (bytevector-address buf)) count
                  0 NULL 0
                  (lambda (errno)
                    (cond ((eqv? errno EAGAIN)
                           (wait-for-readable fd)
                           (recvfrom fd buf start count))
                          ((eqv? errno EINTR)
                           (recvfrom fd buf start count))
                          (else
                           (raise (condition
                                   (make-syscall-error 'recvfrom errno)
                                   (make-irritants-condition (list fd)))))))))
  (define (read! bv start count)
    (recvfrom fd bv start count))
  (define (write! bv start count)
    (sendto fd bv start count))
  (define (close)
    (sys_close fd))
  (let* ((addr (make-bytevector sizeof-sockaddr_un))
         (addrlen (make-bytevector 8)))
    (bytevector-u64-native-set! addrlen 0 (bytevector-length addr))
    (bytevector-u16-native-set! addr offsetof-sockaddr_un-sun_family AF_LOCAL)
    (let ((fn (filename->c-string 'local-connect filename)))
      (bytevector-copy! fn 0
                        addr offsetof-sockaddr_un-sun_path
                        (fxmin (bytevector-length fn)
                               (fx- (fx- sizeof-sockaddr_un offsetof-sockaddr_un-sun_path) 1))))
    (let retry ()
      (sys_connect fd (bytevector-address addr) (bytevector-length addr)
                   (lambda (errno)
                     (cond ((eqv? errno EAGAIN)
                            ;; FIXME: Is this actually correct?
                            ;; Manpages are unclear.
                            (wait-for-writable fd)
                            (retry))
                           ((eqv? errno EINTR)
                            (retry))
                           (else
                            (raise (condition
                                    (make-syscall-error 'connect errno)
                                    (make-irritants-condition (list fd filename))))))))))
  (let ((i (make-custom-binary-input-port (string-append "unix:" filename)
                                          read! #f #f close))
        (o (make-custom-binary-output-port (string-append "unix:" filename)
                                           write! #f #f close)))
    (port-file-descriptor-set! i fd)
    (port-file-descriptor-set! o fd)
    (values i o)))

  )