;; -*- mode: scheme; coding: utf-8 -*-
;; This file is part of yxskaft, a tiny X11 client in Scheme
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>

;; yxskaft is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; yxskaft is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Shared memory

;; This is a fallback library that uses r6rs-pffi and libc. This is
;; not an ideal way to do things, especially with how the pointer is
;; handed off to the application like nothing special.

(library (yxskaft private shm)
  (export
    shmat
    shmctl
    shmdt
    shmget
    NULL
    IPC_PRIVATE
    IPC_CREAT
    IPC_RMID
    put-pixel32)
  (import
    (rnrs)
    (pffi))

(define IPC_PRIVATE 0)
(define IPC_CREAT #o1000)
(define IPC_RMID 0)
(define NULL (integer->pointer 0))

;; FIXME: The very C library itself is seemingly impossible to load
;; through r6rs-pffi on Guile 3
(define lib
  (open-shared-object "libc.so"))

;; (define (shmat shmid *shmaddr shmflg)
;;   )
;; (define (shmctl shmid cmd *buf)
;;   )
;; (define (shmdt *shmaddr)
;;   )
;; (define (shmget key size shmflg)
;;   )

(define shmat
  (foreign-procedure lib pointer shmat (int pointer int)))

(define shmctl
  (foreign-procedure lib pointer shmctl (int int pointer)))

(define shmdt
  (foreign-procedure lib int shmdt (pointer)))

(define shmget
  (foreign-procedure lib int shmget (int int int)))

(define-syntax put-pixel32
  (lambda (x)
    (syntax-case x ()
      ((_ ptr offset v)
       #'(pointer-set-c-uint32! ptr offset v)))))

)
